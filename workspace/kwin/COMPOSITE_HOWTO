This file describes how to set up kwin_composite. Note that since it is still
a work in progress, this file may possibly get out of date at times.


See file HACKING for details on developing KWin, including building
    the kwin_composite branch.
See file COMPOSITE_TODO for a list of things that still need to be done.
See effects/howto.* for a HOWTO on writting effects.
See documentation in source (mainly in scene.cpp) for description
    of the design of the compositing framework.

After starting, KWin's debug output also says which compositing backend it uses, e.g.
"OpenGL compositing". If it does not, most probably your X is not configured properly.

nVidia:
===========

8xxx drivers and newer work, only 9xxx and later drivers provide TFP (texture_from_pixmap)
functionality.

You need in xorg.conf (add to existing sections if they already exist)

Section "Screen"
 Option "AddARGBGLXVisuals" "True"
EndSection

and also the XComposite extension enabled:

Section "Extensions"
 Option "Composite" "Enable"
EndSection

Issues:
-------

When switching away from the X session KWin freezes in nvidia's OpenGL library
    - turn off GLVSync option (see below)
    - fixed with 100.14.23 drivers
TFP mode (see GLMode below) doesn't work with windows with certain sizes (32x32)
    - already fixed in latest drivers
Refresh rate is slower than it should be
    - nvidia drivers use refresh rate to work around some X limitations
    - disable DynamicTwinView (see nvidia README)
    - should(?) be fixed with drivers updated for xrandr1.2
AddARGBGLXVisuals option seems to work only with depth 24
    - change your xorg.conf to use depth 24 (or use a configuration tool)
Xinerama works only when using TwinView, the Xinerama implementation provided by X.Org prevents compositing

check your X log (/var/log/X*.log) for warnings and errors if there are other problems


Intel:
=======

It should work with both the i810 and intel drivers.

You need in xorg.conf the XComposite extension enabled
(add to existing sections if they already exist):

Section "Extensions"
 Option "Composite" "Enable"
EndSection

Also in xorg.conf, you need the DRI module loaded, and AIGLX needs to be enabled:

Section "Module"
 Load "dri"
EndSection
Section "ServerFlags"
 Option "AIGLX"
EndSection

DRI also needs some options in xorg.conf (they should be there by default):

Section "DRI"
 Group "video"
 Mode 0660
EndSection

For SHM, the following in xorg.conf may improve performance:

Section "Screen"
 Option "XaaNoOffscreenPixmaps" "true"
EndSection


Issues:
-------

With the i810 driver, DRI works only on the first running X (if you use secondary X session,
    compositing won't work there), the intel driver doesn't seem to have this problem.
You may get only white rectangles if DRI is not enabled or fails to initialize
    - check your X log (/var/log/X*.log) and your xorg.conf
Window contents updating may not work properly in secondary X session (likely X/driver bug)
Xinerama does not work with the Xinerama implementation in X.Org
    - you need to use solutions like MergedFB

check your X log (/var/log/X*.log) for warnings and errors if there are other problems


ATI:
========

The radeon driver should work for R200 chips, it's worse with R300 chips.
The fglrx driver requires XGL for versions older than 8.42.3.

Refer to the Intel section, setting up should be similar.



Configuration options:
======================

There is configuration module for most options. If you need to manually disable compositing,
use the following command:
kwriteconfig --file kwinrc --group Compositing --key Enabled --type bool false

ShowFPSEffect:
Options are in group [EffectShowFps] in kwinrc config file:
Alpha=<number 0.0-1.0> - transparency
X=<number> - X position, negative is from the right edge, -10000 is exactly right edge
Y=<number> = Y position, the same like X


Effects:
========

Effects can be enabled/disabled in KConfig module accessible by right-clicking
on a window title, choosing "Configure Window Behaviour" and then
"Window Effects".
