
project(kstyle-light)
add_definitions(-DQT_PLUGIN -DQT3_SUPPORT )

set(light_PART_SRCS
   light.cpp
   lightstyle-v2.cpp
   lightstyle-v3.cpp
)

kde4_add_plugin(light ${light_PART_SRCS})
target_link_libraries(light  ${KDE4_KDECORE_LIBS} )
install(TARGETS light  DESTINATION ${PLUGIN_INSTALL_DIR}/plugins/styles )
