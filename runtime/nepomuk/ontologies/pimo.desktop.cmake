[Desktop Entry]
Version=1.0
Name=PIMO
Comment=The Personal Information Model Ontology can be used to express personal information of individuals.
URL=http://www.semanticdesktop.org/ontologies/2007/11/01/pimo#
Path=${DATA_INSTALL_DIR}/nepomuk/ontologies/pimo.trig
MimeType=application/x-trig
Type=Data
