[Desktop Entry]
Version=1.0
Name=The Xesam meta data ontology
Comment=The Xesam meta data ontology provides classes and properties for desktop file meta data
URL=http://freedesktop.org/standards/xesam/1.0/core#
Path=${DATA_INSTALL_DIR}/nepomuk/ontologies/xesam.rdfs
MimeType=application/rdf+xml
Type=Data
