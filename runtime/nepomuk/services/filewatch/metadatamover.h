/* This file is part of the KDE Project
   Copyright (c) 2009 Sebastian Trueg <trueg@kde.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef _NEPOMUK_METADATA_MOVER_H_
#define _NEPOMUK_METADATA_MOVER_H_

#include <QtCore/QThread>
#include <QtCore/QMutex>
#include <QtCore/QWaitCondition>
#include <QtCore/QQueue>
#include <QtCore/QPair>

#include <KUrl>

namespace Soprano {
    class Model;
}

namespace Nepomuk {
    class MetadataMover : public QThread
    {
        Q_OBJECT

    public:
        MetadataMover( Soprano::Model* model, QObject* parent = 0 );
        ~MetadataMover();

    public Q_SLOTS:
        void moveFileMetadata( const KUrl& from, const KUrl& to );
        void removeFileMetadata( const KUrl& file );
        void removeFileMetadata( const KUrl::List& files );

        void stop();

    private:
        void run();

        void removeMetadata( const KUrl& );
        void updateMetadata( const KUrl& from, const KUrl& to );

        // if the second url is empty, just delete the metadata
        QQueue<QPair<KUrl, KUrl> > m_updateQueue;
        QMutex m_queueMutex;
        QWaitCondition m_queueWaiter;
        bool m_stopped;

        Soprano::Model* m_model;

        QUrl m_strigiParentUrlUri;
    };
}

#endif
